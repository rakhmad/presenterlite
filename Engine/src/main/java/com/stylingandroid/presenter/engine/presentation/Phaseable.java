package com.stylingandroid.presenter.engine.presentation;

public interface Phaseable
{
	public boolean setPhase( final int phase );
	public int getLastPhase();
}
