package com.stylingandroid.presenter.engine.presentation;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.stylingandroid.presenter.R;

public class SlideFragment extends Fragment
{
	private int resource;

	public SlideFragment( int resource )
	{
		this.resource = resource;
	}
	
	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState )
	{
		View v = inflater.inflate( resource, container, false );
		if( container instanceof DisplayLayout )
		{
			((DisplayLayout)container).setCurrentSlide( (SlideLayout )v.findViewById( R.id.slide ) );
		}
		return v;
	}
}
