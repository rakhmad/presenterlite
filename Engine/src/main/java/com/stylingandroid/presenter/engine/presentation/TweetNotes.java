package com.stylingandroid.presenter.engine.presentation;

public interface TweetNotes
{
	public void tweet( String tweet );
	public void notes( String note );
}
